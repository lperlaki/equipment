import db from '.'
import bcrypt from 'bcrypt'

const table = 'users'

export function all () {
  return db.getAll(table)
}

export async function add (params) {
  if (params.password) params.password = await bcrypt.hash(params.password, 10)
  return db.insert(table, params)
}

export function get (id) {
  return db.get(table, id)
}

export async function upd (id, params) {
  if (params.password) params.password = await bcrypt.hash(params.password, 10)
  return db.update(table, id, params)
}

export function del (id) {
  return db.delete(table, id)
}

export function findByEmail (email) {
  return db.one('SELECT users.id, concat(first_name, \' \', last_name) as name, first_name, last_name, email, password, roles.name as role from $1~ JOIN roles ON users.role_id = roles.id WHERE users.email = $2', [table, email])
}

export function findById(id) {
  return db.one('SELECT users.id, concat(first_name, \' \', last_name) as name, first_name, last_name, email, roles.name as role from $1~ JOIN roles ON users.role_id = roles.id WHERE users.id = $2', [table, id])
}

export default { all, add, get, upd, del }
