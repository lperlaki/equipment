import Vue from 'vue'
import Router from 'vue-router'
import { Main, Login, Home, About, Settings, Items, Users } from './views'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      component: Login
    },
    {
      path: '/',
      name: 'main',
      component: Main,
      children: [
        {
          path: '/',
          name: 'home',
          component: Home
        }, {
          path: 'about',
          name: 'about',
          component: About
        }, {
          path: 'settings',
          name: 'settings',
          component: Settings
        }, {
          path: 'items',
          name: 'items',
          component: Items
        }, {
          path: 'users',
          name: 'users',
          component: Users
        }
      ]
    }
  ]
})
