import express from 'express'
import session from 'express-session'
import passport from 'passport'
import pLocal from 'passport-local'
import history from 'connect-history-api-fallback'
import cookiParser from 'cookie-parser'
import bcrypt from 'bcrypt'


const Strategy = pLocal.Strategy

// Passport

import { add, findByEmail, findById } from './models/user'

passport.use('local', new Strategy(async function (username, password, done) {
  try {
    let user = await findByEmail(username)
    let match = await bcrypt.compare(password, user.password)
    if (!user || !match) return done(null, false)
    return done(null, user)
  } catch (error) {
    return done(error)
  }
}))

passport.serializeUser((user, done) => {
  done(null, user.id)
})

passport.deserializeUser(async function (id, done) {
  try {
    let user = await findById(id)
    done(null, user)
  } catch (error) {
    done(error)
  }
})



// Get our API routes
import {api, auth} from './routes'

const port = process.env.PORT || '3000'

const app = express()

// Parsers for POST data
app.use(express.json())
app.use(cookiParser());
app.use(session({
  secret: process.env.SESSION_SECRET || 'awesomecookiesecret',
  resave: false,
  saveUninitialized: false
}))
app.use(passport.initialize());
app.use(passport.session());

app.set('port', port)

app.use('/auth', auth)

app.use((req, res, next) => {
  if (req.originalUrl == '/login' || req.originalUrl.startsWith('/auth') || (req.path.lastIndexOf('.') > req.path.lastIndexOf('/'))) return next()
  if (!req.user) return res.redirect('/login')
  next()
})
 

// Set our api routes
app.use('/api', api)

// Point static path to dist
app.use('/', history(), express.static('dist'))


/**
 * Listen on provided port, on all network interfaces.
 */
app.listen(port, () => console.log(`API running on localhost:${port}`))

export default app
