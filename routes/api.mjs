import express from 'express'
import { User, Item, Role } from '../models'

const router = express.Router()

/* GET home page. */
router.get('/', function (req, res) {
  res.send('API Works')
})

router.route('/items')
  .get(async function (req, res) {
    res.send(await Item.all())
  })
  .post(async function (req, res) {
    res.send(await Item.add(req.body))
  })

router.route('/items/:id')
  .get(async function (req, res) {
    res.send(await Item.get(req.params.id))
  })
  .put(async function (req, res) {
    res.send(await Item.upd(req.params.id, req.body))
  })
  .delete(async function (req, res) {
    res.send(await Item.del(req.params.id, req.body))
  })

router.route('/users')
  .get(async function (req, res) {
    res.send(await User.all())
  })
  .post(async function (req, res) {
    res.send(await User.add(req.body))
  })

router.route('/users/:id')
  .get(async function (req, res) {
    res.send(await User.get(req.params.id))
  })
  .put(async function (req, res) {
    res.send(await User.upd(req.params.id, req.body))
  })
  .delete(async function (req, res) {
    res.send(await User.del(req.params.id, req.body))    
  })

router.route('/roles')
  .get(async function (req, res) {
    res.send(await Role.all())
  })
  

export default router
