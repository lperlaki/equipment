CREATE TABLE categories
(
  id SERIAL PRIMARY KEY,
  parent INT REFERENCES categories,
  name VARCHAR(50) NOT NULL
);

CREATE TABLE locatoins
(
  id SERIAL PRIMARY KEY ,
  room SMALLINT NOT NULL,
  locker SMALLINT NOT NULL,
  shelf SMALLINT NOT NULL,
  spot SMALLINT NOT NULL
);

CREATE TABLE items (
  id SERIAL PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  category_id INT REFERENCES categories,
  description TEXT,
  image VARCHAR(100),
  info JSON,
  location_id INT REFERENCES locatoins
);

CREATE TABLE roles (
  id SERIAL PRIMARY KEY,
  name VARCHAR(10) NOT NULL,
  description VARCHAR(255)
)

CREATE TABLE classes (
  id SERIAL PRIMARY KEY,
  name VARCHAR(10) NOT NULL
);

CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  email VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  role_id INT REFERENCES roles,
  class_id INT REFERENCES classes
);

CREATE TABLE rented (
  id SERIAL PRIMARY KEY,
  item_id INT REFERENCES items,
  user_id INT REFERENCES users,
  reserved TIME,
  rented TIME,
  returned TIME
);
